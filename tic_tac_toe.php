<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "digio";
$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) { die("Connection failed: " . $conn->connect_error); } 

?><!DOCTYPE html>

<html>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.min.css">

<style type="text/css">
	*{ margin: 0; padding: 0; }
	html{ background-color: #EAEDED; }
	body { width: 765px; margin: 0 auto; background-color: #FFFFFF; padding: 10px;}
	table td{ width: 50px; height: 50px;cursor: pointer; text-align: center; font-weight: bold; }
</style>


<?php

$flg = $num = $replay = $last_id = 0;
if(!isset($_POST['fn'])) $_POST['fn'] = '';

if($_POST['fn'] == 'start'){
	if(isset($_POST['size'])){
		$sql = "INSERT INTO game (size) VALUES (".$_POST['size'].")";
		if ($conn->query($sql) === TRUE) { $flg = 1; $last_id = $conn->insert_id; }
		$num=$_POST['size'];
	}
}else if($_POST['fn'] == 'replay'){
	$sql = "select * from game where id = ".$_POST['gameid'];
	$data = $conn->query($sql);
	foreach ($data as $val) {
		$num=$val['size'];
		$last_id = $val['id'];
	}
	$flg = $replay = 1;
	//echo print_r($data);
}



$sql2 = "select * from game";
$data2 = $conn->query($sql2);


?>



<body>




<h1>TIC TAC TOE</h1>


<div>
	<form action="" method="post" id="">
		<div>เลือก Replay : 
			<select name="gameid">
				<?php foreach ($data2 as $key => $val) { ?>
					<option value="<?= $val['id'] ?>"><?= $val['id'] ?></option>
				<?php } ?>
			</select>
		</div>
		<input type="hidden" name="fn" value="replay">
		<input type="submit" id="btnReplay" value="Replay">
	</form>


	<form action="" method="post">
		<div>กำหนดขนาด : <input type="text" name="size"></div>
		<input type="hidden" name="fn" value="start">
		<input type="submit" value="Submit">
	</form>


	<br><br>
	<div class="game">
		<div class="msg"></div>
		<input type="hidden" id="replay" value="<?= $replay; ?>">
		<input type="hidden" id="size" value="<?= $num; ?>">
		<input type="hidden" id="gameid" value="<?= $last_id; ?>">
		<table border="1" collapse="collapse">
		<?php 
			if($flg){
				for($i=0; $i<$num; $i++){
					echo "<tr>";
					for($j=0; $j<$num; $j++){
						echo "<td class='cell' data-h='$i' data-v='$j'></td>";
					}
					echo "</tr>";
				}
			}
		?>
		</table>
	</div>
</div>




<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript">
	var player = 1;
	var size = parseInt($('#size').val());
	var gameid = parseInt($('#gameid').val());

	var state = new Array(size);
	for (let i = 0; i < state.length; i++) { state[i] = new Array(size); }
	//console.log(state);




	function checkWinGame(p, cell){
		var flg=1;
		var player = (p==1)? 'O':'X';

		//console.log(p, cell, state);

		jQuery.each( state[cell.h], function( i, val ) { 
			if(val!=p){ flg=0; return true; }			
		});
		//console.log('flg',flg);
		if(flg){ $('.msg').html('Player: '+player+' Win'); $(".cell").off("click"); return true; }
		
		flg=1
		for(let i=0; i<size; i++){ 
			if(state[i][cell.v]!=p) { flg=0; break; }
		}
		//console.log('flg2',flg);
		if(flg){ $('.msg').html('Player: '+player+' Win'); $(".cell").off("click"); return true; }

		flg=1
		for(let i=0; i<size; i++){ 
			if(state[i][i]!=p) { flg=0; break; }
		}
		//console.log('flg3',flg);
		if(flg){ $('.msg').html('Player: '+player+' Win'); $(".cell").off("click"); return true; }


		flg=1
		for(let i=0,j = size; i<size; i++){ j--;
			if(state[j][i]!=p) { flg=0; break; }
		}
		//console.log('flg4',flg);
		if(flg){ $('.msg').html('Player: '+player+' Win'); $(".cell").off("click"); return true; }


		var tmp=0;
		jQuery.each( state, function( i, val ) { 
			jQuery.each( val, function( i, val2 ) {	
				if(!val2) tmp++;
			});	
		});
		if(tmp == 0){ $('.msg').html('Game Draw'); $(".cell").off("click"); return true; }
		//console.log('flg5',flg);

	}



	$('.cell').click(function(){
		if($(this).html() == ''){
			state[this.dataset.h][this.dataset.v] = player;	

			var pp = (player==1)? 'O':'X';
			$.ajax({
				method: "POST",
				url: "ajax.php",
				data: { fn:'save', game_id: gameid, player:pp, x_line: this.dataset.h, y_line: this.dataset.v }
			}).done(function(data) {
				console.log('done');
			});

			if(player == 1){ $(this).html('O'); checkWinGame(player, this.dataset); player=2; }
			else if(player == 2){ $(this).html('X'); checkWinGame(player, this.dataset); player=1; }			
		}
	})




	if($('#replay').val() > 0 ){ console.log('replay');
		$.ajax({
			method: "POST",
			url: "ajax.php",
			data: { fn:'view', game_id: gameid }
		}).done(function(data) {
			var obj = JSON.parse(data);

			$(".cell").off("click");

			jQuery.each( obj, function( i, val ) { 
				setTimeout(function(){
					player = (val.player=='O')? 1:2;
					state[val.x_line][val.y_line] = player;	

					var cell = {'h':val.x_line, 'v':val.y_line};

					if(player == 1){ $('.cell[data-h='+val.x_line+'][data-v='+val.y_line+']').html('O'); checkWinGame(player, cell); }
					else if(player == 2){ $('.cell[data-h='+val.x_line+'][data-v='+val.y_line+']').html('X'); checkWinGame(player, cell); }		
					//console.log('done',val, val.id, cell);
				}, i*1000)				
			});
		});
	}


</script>

</body>
</html>